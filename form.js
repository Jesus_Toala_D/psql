form.addEventListener('submit', e => {
	e.preventDefault();
	const res = checkInputs();	
	if(res==0){
			const jsonUsuario = {
				"cedula": cedula.value,
				"apellidos": apellidos.value,
				"nombres": nombres.value,
				"direccion": direccion.value,
				"semestre": semestre.value,
                "paralelo": paralelo.value,
                "email": email.value
			}		
			fetch(`${API_URL}/add`, {
				method: 'POST',
				headers: {               
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(jsonUsuario)
			})
				.then(response => response.text())
				.then(text => {
					alert(text)
					location.href = "index.html"
				})
		}		
});